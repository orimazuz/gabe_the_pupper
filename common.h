//
//  common.h
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef common_h
#define common_h

#ifdef _WIN32
# include <Windows.h>
#endif

#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifndef PI
#define PI 3.14159265
#endif

#define WINDOW_HEIGHT (720)
#define WINDOW_WIDTH (1080)

#define AMB_METER_MIN_X (20)
#define AMB_METER_MIN_Y (WINDOW_HEIGHT - 65)
#define AMB_METER_MAX_X (300)
#define AMB_METER_MAX_Y (WINDOW_HEIGHT - 100)

#define YMAX_ANGLE (40)
#define WALL_BUFFER (15)

#define MIN_ATTEN (0.006)
#define MAX_ATTEN (0.08)

#endif /* common_h */
