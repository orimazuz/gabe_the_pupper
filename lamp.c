//
//  lamp.c
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#include "common.h"
#include "lamp.h"

extern GLfloat ambient_intensity;

void drawLamp(float *emmition)
{
    GLfloat spec[] = {0.3, 0.3, 0.3, 1};
    GLfloat lamp_body[] = {0.1, 0.1, 0.3, 1};
    GLfloat default_emmition[]  = {0,0,0,0};

    GLfloat lamp_body_amb[] = 
        {lamp_body[0] * ambient_intensity, lamp_body[1] * ambient_intensity, lamp_body[2] * ambient_intensity, 1};
    GLfloat lamp_head_amb[] = 
        {0.2 * ambient_intensity, 0.3 * ambient_intensity, 0.3 * ambient_intensity, 1};
    
    glPushMatrix();
    
    glMaterialfv(GL_FRONT, GL_SPECULAR, spec);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, lamp_body);
    glMaterialfv(GL_FRONT, GL_AMBIENT, lamp_body_amb);
    
    // body
    glPushMatrix();
    glScaled(2, 0.5, 2);
    glutSolidSphere(3, 200, 100);
    glPopMatrix();
    glPushMatrix();
    glRotatef(-90, 1, 0 ,0);
    GLUquadric* q = gluNewQuadric();
    gluCylinder(q, 1, 1, 19, 4, 4);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(0, 20, 0);
    glMaterialfv(GL_FRONT, GL_AMBIENT, lamp_head_amb);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, emmition);
    glMaterialfv(GL_FRONT, GL_EMISSION, emmition);
    glutSolidSphere(4, 200, 100);
    glMaterialfv(GL_FRONT, GL_EMISSION, default_emmition);
    glPopMatrix();
    
    glPopMatrix();
    
    gluDeleteQuadric(q);
}
