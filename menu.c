//
//  menu.c
//  mmn17
//
//  Created by Ori Mazuz on 15/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#include "common.h"
#include "menu.h"

extern GLfloat ambient_intensity;
extern unsigned int help_tex_width, help_tex_height;
extern unsigned char *help_tex;
extern unsigned int amb_tex_width, amb_tex_height;
extern unsigned char *amb_tex;
extern unsigned char *floor_tex;
extern unsigned char *wall_tex;
extern int menu_help, menu_ambient;

void menu(int value)
{
    switch (value)
    {
        case M_HELP:
            menu_help = 1;
			menu_ambient = 0;
            break;
        case M_AMBIENT:
            menu_ambient = 1;
			menu_help = 0;
            break;
        case M_QUIT:
            // free allocated textures
            if (floor_tex)
                free(floor_tex);
            if (wall_tex)
                free(wall_tex);
            if (amb_tex)
                free(amb_tex);
            if (help_tex)
                free(help_tex);
            exit(0);
            break;
        default:
            break;
    }
}

// draws 2d picture of help screen on the 3d view
void print_help()
{
    glMatrixMode(GL_PROJECTION);
    // save projection matrix
    glPushMatrix();
    glLoadIdentity();
    // move to 2d ortho projection
    gluOrtho2D(0, 1080, 0, 720);
    
    glMatrixMode(GL_MODELVIEW);
    // save modelview matrix
    glPushMatrix();
    glLoadIdentity();
    // draw help picture texture (2d)
    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, help_tex_width, help_tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, help_tex);
    glColor4f(1, 1, 1, 0.8);
    glEnable(GL_BLEND);
    glBegin(GL_QUADS);
    glTexCoord2f(1, 1); glVertex2f(1080, 720);
    glTexCoord2f(0, 1); glVertex2f(0, 720);
    glTexCoord2f(0, 0); glVertex2f(0, 345);
    glTexCoord2f(1, 0); glVertex2f(1080, 345);
    glEnd();
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    
    glMatrixMode(GL_PROJECTION);
    // return to 3d projection
    glPopMatrix();
    
    glMatrixMode(GL_MODELVIEW);
    // pop saved 3d modelview
    glPopMatrix();
}

// same as print_help, but with interactive slide bar
void adjust_ambient()
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, WINDOW_WIDTH, 0, WINDOW_HEIGHT);
    
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    
    glDisable(GL_LIGHTING);
    
    // draw yellow slide bar according to the ambient intensity
    glColor4f(0.7, 0.7, 0.4, 1);
    glBegin(GL_QUADS);
    glVertex2f(AMB_METER_MAX_X * ambient_intensity, AMB_METER_MIN_Y);
    glVertex2f(AMB_METER_MIN_X, AMB_METER_MIN_Y);
    glVertex2f(AMB_METER_MIN_X, AMB_METER_MAX_Y);
    glVertex2f(AMB_METER_MAX_X * ambient_intensity, AMB_METER_MAX_Y);
    glEnd();
    
    // draw global white bar
    glColor4f(1, 1, 1, 1);
    glBegin(GL_QUADS);
    glVertex2f(AMB_METER_MAX_X, AMB_METER_MIN_Y);
    glVertex2f(AMB_METER_MIN_X, AMB_METER_MIN_Y);
    glVertex2f(AMB_METER_MIN_X, AMB_METER_MAX_Y);
    glVertex2f(AMB_METER_MAX_X, AMB_METER_MAX_Y);
    glEnd();
    
    // draw picture
    glEnable(GL_TEXTURE_2D);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, amb_tex_width, amb_tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, amb_tex);
    glColor4f(1, 1, 1, 0.8);
    glEnable(GL_BLEND);
    glBegin(GL_QUADS);
    glTexCoord2f(1, 1); glVertex2f(320, WINDOW_HEIGHT);
    glTexCoord2f(0, 1); glVertex2f(0, WINDOW_HEIGHT);
    glTexCoord2f(0, 0); glVertex2f(0, WINDOW_HEIGHT - 128);
    glTexCoord2f(1, 0); glVertex2f(320, WINDOW_HEIGHT - 128);
    glEnd();
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    
    glEnable(GL_LIGHTING);
    
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}
