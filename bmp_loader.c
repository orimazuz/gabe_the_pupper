//
//  bmp_loader.c
//  mmn17
//
//  Created by Ori Mazuz on 13/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#include "bmp_loader.h"
#include <stdio.h>
#include <stdlib.h>

unsigned char * load_bmp(char *filename, unsigned int *width, unsigned int *height)
{
    // Data read from the header of the BMP file
    unsigned char header[54]; // Each BMP file begins by a 54-bytes header
    unsigned int dataPos;     // Position in the file where the actual data begins
    unsigned int imageSize;   // = width*height*3
    // Actual RGB data
    unsigned char * data;
    
    // Open the file
#ifdef _WIN32
	FILE * file;
	fopen_s(&file, filename, "rb");
#else
    FILE * file = fopen(filename, "rb");
#endif
    if (!file) {
        perror("Image could not be opened\n");
        return 0;
    }
    
    if ( fread(header, 1, 54, file) != 54 ) { // If not 54 bytes read : problem
        printf("Not a correct BMP file\n");
        return 0;
    }
    
    if ( header[0]!='B' || header[1]!='M' ) {
        printf("Not a correct BMP file\n");
        return 0;
    }
    
    // Read ints from the byte array
    dataPos    = *(int*)&(header[0x0A]);
    imageSize  = *(int*)&(header[0x22]);
    *width      = *(int*)&(header[0x12]);
    *height     = *(int*)&(header[0x16]);
    
    if (imageSize == 0)
        imageSize = *width * *height * 3; // 3 : one byte for each Red, Green and Blue component
    if (dataPos == 0)
        dataPos = 54; // The BMP header is done that way
    
    // Create a buffer
    data = (unsigned char*)malloc(imageSize);
    
    // Read the actual data from the file into the buffer
    fread(data,1,imageSize,file);
    
    //Everything is in memory now, the file can be closed
    fclose(file);
    
    return data;
}
