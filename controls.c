//
//  controls.c
//  mmn17
//
//  Created by Ori Mazuz on 15/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#include "controls.h"

extern int first_person, third_person;
extern int menu_help, menu_ambient;
extern int camera_moving;
extern int startx, starty;
extern int move_left, move_right, move_foreward, move_backward;
extern int player_dog, player_lamp1, player_lamp2;
extern int toggle_dog_moving, toggle_head_moving, toggle_tail_moving;
extern GLfloat angleX, angleY;
extern GLfloat ambient_intensity;
extern GLfloat blue_light_atten, yellow_light_atten, ceiling_light_atten;

void mouse(int button, int state, int x, int y)
{
    if (menu_help) {
        menu_help = 0;
    }
    // on "adjust ambient light menu", set the ambient light according to the mouse's position
    if (menu_ambient) {
		if (x > AMB_METER_MIN_X && x < AMB_METER_MAX_X &&
			y > WINDOW_HEIGHT - AMB_METER_MIN_Y && y < WINDOW_HEIGHT - AMB_METER_MAX_Y) {
			ambient_intensity = ((GLfloat)x - AMB_METER_MIN_X) / (AMB_METER_MAX_X - AMB_METER_MIN_X);
		}
		else {
            menu_ambient = 0;
        }
    }
    if (button == GLUT_LEFT_BUTTON) {
        if (!menu_ambient) {
            if (state == GLUT_DOWN) {
                camera_moving = 1;
                startx = x;
                starty = y;
            }
            if (state == GLUT_UP) {
                camera_moving = 0;
            }
        }
    }
}

void motion(int x, int y)
{
    // on "adjust ambient light menu", set the ambient light according to the mouse's position
    if (menu_ambient) {
        if (x > AMB_METER_MIN_X && x < AMB_METER_MAX_X &&
            y > WINDOW_HEIGHT - AMB_METER_MIN_Y && y < WINDOW_HEIGHT - AMB_METER_MAX_Y) {
            ambient_intensity = ((GLfloat)x - AMB_METER_MIN_X) / (AMB_METER_MAX_X - AMB_METER_MIN_X);
        }
    }
    if (camera_moving) {
        angleX = angleX + (x - startx);
        angleY = angleY + (y - starty);
        if (YMAX_ANGLE < angleY)
            angleY = YMAX_ANGLE;
        if (-YMAX_ANGLE > angleY)
            angleY = -YMAX_ANGLE;
        startx = x;
        starty = y;
    }
}

void special_keys(int key, int xx, int yy)
{
    switch (key) {
        case GLUT_KEY_LEFT :
            move_left = 1;
            break;
        case GLUT_KEY_RIGHT :
            move_right = 1;
            break;
        case GLUT_KEY_UP :
            move_foreward = 1;
            break;
        case GLUT_KEY_DOWN :
            move_backward = 1;
            break;
    }
}

void special_up_keys(int key, int xx, int yy)
{
    switch (key) {
        case GLUT_KEY_LEFT :
            move_left = 0;
            break;
        case GLUT_KEY_RIGHT :
            move_right = 0;
            break;
        case GLUT_KEY_UP :
            move_foreward = 0;
            break;
        case GLUT_KEY_DOWN :
            move_backward = 0;
            break;
    }
}

void keyboard(unsigned char key, int x, int y)
{
    const GLfloat factor = 0.002;
    switch (key) {
        case 'q':
		case 'Q':
            ceiling_light_atten -= factor;
            if (ceiling_light_atten < MIN_ATTEN)
                ceiling_light_atten = MIN_ATTEN;
            glEnable(GL_LIGHT0);
            break;
        case 'a':
		case 'A':
            ceiling_light_atten += factor;
            if (ceiling_light_atten > MAX_ATTEN) {
                ceiling_light_atten = MAX_ATTEN;
                glDisable(GL_LIGHT0);
            }
            break;
        case 'w':
		case 'W':
            yellow_light_atten -= factor;
            if (yellow_light_atten < MIN_ATTEN)
                yellow_light_atten = MIN_ATTEN;
            glEnable(GL_LIGHT1);
            break;
        case 's':
		case 'S':
            yellow_light_atten += factor;
            if (yellow_light_atten > MAX_ATTEN) {
                yellow_light_atten = MAX_ATTEN;
                glDisable(GL_LIGHT1);
            }
            break;
        case 'e':
		case 'E':
            blue_light_atten -= factor;
            if (blue_light_atten < MIN_ATTEN)
                blue_light_atten = MIN_ATTEN;
            glEnable(GL_LIGHT2);
            break;
        case 'd':
		case 'D':
            blue_light_atten += factor;
            if (blue_light_atten > MAX_ATTEN) {
                blue_light_atten = MAX_ATTEN;
                glDisable(GL_LIGHT2);
            }
            break;
        case 'f':
		case 'F':
            init_first_person();
            break;
        case 't':
		case 'T':
            init_third_person();
            break;
        case '1':
            switch_dog_player();
            break;
        case '2':
            switch_lamp1_player();
            break;
        case '3':
            switch_lamp2_player();
            break;
        case 'h':
		case 'H':
            toggle_head();
            break;
        case 'j':
		case 'J':
            toggle_tail();
            break;
        default:
            break;
    }
}
