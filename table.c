//
//  table.c
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#include "common.h"
#include "table.h"

extern GLfloat ambient_intensity;

void drawTable(void)
{
    GLfloat table_diff[] = {0.6, 0.3, 0.2, 1.0};
    GLfloat teapot_diff[] = {0.1, 0.3, 0.2, 1.0};
    GLfloat teapot_spec[] = {1, 1, 1, 1};
    GLfloat teapot_amb[] = 
        {teapot_diff[0] * ambient_intensity, teapot_diff[1] * ambient_intensity, teapot_diff[2] * ambient_intensity, 1};
    GLfloat table_amb[] = 
        {table_diff[0] * ambient_intensity, table_diff[1] * ambient_intensity, table_diff[2] * ambient_intensity, 1};
    
    glMaterialfv(GL_FRONT, GL_DIFFUSE, table_diff);
    glMaterialfv(GL_FRONT, GL_AMBIENT, table_amb);
    
    // legs
    glPushMatrix();
    glTranslatef(9, 0, 9);
    glRotatef(-90, 1, 0 ,0);
    GLUquadric* q1 = gluNewQuadric();
    gluCylinder(q1, 1, 1, 10, 4, 4);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(-9, 0, 9);
    glRotatef(-90, 1, 0 ,0);
    GLUquadric* q2 = gluNewQuadric();
    gluCylinder(q2, 1, 1, 10, 4, 4);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0, 0, -7.3);
    glRotatef(-90, 1, 0 ,0);
    GLUquadric* q3 = gluNewQuadric();
    gluCylinder(q3, 1, 1, 10, 4, 4);
    glPopMatrix();
    
    // top
    glPushMatrix();
    glTranslatef(0, 10, 3);
    glRotatef(-90, 1, 0 ,0);
    GLUquadric* q4 = gluNewQuadric();
    gluCylinder(q4, 13, 13, 1, 200, 200);
    glPopMatrix();
    
    glPushMatrix();
    glTranslatef(0, 11, 3);
    glRotatef(-90, 1, 0 ,0);
    GLUquadric* q5 = gluNewQuadric();
    gluDisk(q5, 0, 13, 200, 200);
    glPopMatrix();
    
    // teapot
    glMaterialfv(GL_FRONT, GL_DIFFUSE, teapot_diff);
    glMaterialfv(GL_FRONT, GL_AMBIENT, teapot_amb);
    // make it metalic
    glMaterialfv(GL_FRONT, GL_SPECULAR, teapot_spec);
    glPushMatrix();
    glTranslatef(0, 15, 2);
    glutSolidTeapot(5);
    glPopMatrix();
    
    gluDeleteQuadric(q1);
    gluDeleteQuadric(q2);
    gluDeleteQuadric(q3);
    gluDeleteQuadric(q4);
    gluDeleteQuadric(q5);
}
