//
//  doggo.c
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#include "common.h"
#include "doggo.h"

extern int toggle_dog_moving;
extern int toggle_head_moving;
extern int toggle_tail_moving;
extern int move_left;
extern int move_right;
extern int move_foreward;
extern int move_backward;

extern GLfloat tail_angleY;
extern GLfloat tail_angleZ;
extern GLfloat head_angleY;
extern GLfloat head_angleZ;

extern GLfloat ambient_intensity;

GLfloat right_legs_angle = 0.0f;
GLfloat left_legs_angle = 0.0f;

// when moving, the legs should move back and fourth.
void leg_animation()
{
    static GLfloat leg_angle_inc = 5;
    
    if (left_legs_angle == 25)
        leg_angle_inc = -5;
    if (left_legs_angle == -25)
        leg_angle_inc = 5;
    
    if (toggle_dog_moving &&
        (move_left || move_right || move_foreward || move_backward)) {
        left_legs_angle += leg_angle_inc;
        right_legs_angle -= leg_angle_inc;
    }
    else {
        left_legs_angle = 0;
        right_legs_angle = 0;
    }
}

void drawDoggo(void)
{
    GLfloat dog_color1[] = {0.5, 0.3, 0.1, 1.0};
    GLfloat dog_color2[] = {0.6, 0.4, 0.1, 1.0};
    GLfloat dog_color3[] = {0.1, 0.1, 0.1, 1.0};
    GLfloat spec[] = {0.0, 0.0, 0.0, 1.0};

    GLfloat dog_color_amb1[] = 
        {ambient_intensity * dog_color1[0], ambient_intensity * dog_color1[1], ambient_intensity * dog_color1[2]};
    GLfloat dog_color_amb2[] = 
        {ambient_intensity * dog_color2[0], ambient_intensity * dog_color2[1], ambient_intensity * dog_color2[2]};
    GLfloat dog_color_amb3[] = 
        {ambient_intensity * dog_color3[0], ambient_intensity * dog_color3[1], ambient_intensity * dog_color3[2]};
    
    leg_animation();

    glPushMatrix();
    
    glMaterialfv(GL_FRONT, GL_SPECULAR, spec);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color1);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb1);
    glTranslatef(0, 10, 0);
    
    // body
    glPushMatrix();
    glScaled(2, 1, 1);
    glutSolidSphere(5, 200, 100);
    glPopMatrix();
    
    // front legs
    glPushMatrix();
    glRotatef(right_legs_angle, 0, 0, 1);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color2);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb2);
    glPushMatrix();
    glTranslatef(6, -4, 2);
    glScaled(0.5, 2.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color1);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb1);
    glPushMatrix();
    glTranslatef(6.5, -9, 2);
    glScaled(0.7, 0.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    glPopMatrix();
    
    glPushMatrix();
    glRotatef(left_legs_angle, 0, 0, 1);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color2);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb2);
    glPushMatrix();
    glTranslatef(6, -4, -2);
    glScaled(0.5, 2.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color1);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb1);
    glPushMatrix();
    glTranslatef(6.5, -9, -2);
    glScaled(0.7, 0.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    glPopMatrix();
    
    // back legs
    glPushMatrix();
    glRotatef(left_legs_angle, 0, 0, 1);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color2);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb2);
    glPushMatrix();
    glTranslatef(-6, -4.5, 2);
    glScaled(0.5, 2.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color1);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb1);
    glPushMatrix();
    glTranslatef(-5.5, -9, 2);
    glScaled(0.7, 0.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    glPopMatrix();
    
    glPushMatrix();
    glRotatef(right_legs_angle, 0, 0, 1);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color2);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb2);
    glPushMatrix();
    glTranslatef(-6, -4.5, -2);
    glScaled(0.5, 2.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color1);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb1);
    glPushMatrix();
    glTranslatef(-5.5, -9, -2);
    glScaled(0.7, 0.5, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    glPopMatrix();
    
    // tail
    glPushMatrix();
    glTranslatef(-9, 2, 0);
    glTranslatef(0, -3, 0);
    glRotated(tail_angleZ, 1, 0, 0);
    glRotated(tail_angleY, 0, 0, 1);
    glTranslatef(0, 3, 0);
    glScaled(0.5, 2, 0.5);
    glutSolidSphere(2, 10, 100);
    glPopMatrix();
    
    // head
    glPushMatrix();
    glTranslatef(8, 2, 0);
    glRotated(head_angleZ, 1, 0, 0);
    glRotated(head_angleY, 0, 0, 1);
    glTranslatef(-8, -2, 0);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color2);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb2);
    glPushMatrix();
    glTranslatef(8, 2, 0);
    glutSolidSphere(5, 10, 100);
    glPopMatrix();
    
    // ears
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color1);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb1);
    glPushMatrix();
    glTranslatef(9, 3, 4);
    glScaled(1, 2, 2);
    glRotated(45, 0, 1, 0);
    glutSolidSphere(1.3, 3, 4);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(9, 3, -4);
    glScaled(1, 2, 2);
    glRotated(-45, 0, 1, 0);
    glutSolidSphere(1.3, 3, 4);
    glPopMatrix();
    
    // eyes
    glMaterialfv(GL_FRONT, GL_DIFFUSE, dog_color3);
    glMaterialfv(GL_FRONT, GL_AMBIENT, dog_color_amb3);
    glPushMatrix();
    glTranslatef(11.8, 4, 2);
    glutSolidSphere(0.5, 4, 4);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(11.8, 4, -2);
    glutSolidSphere(0.5, 4, 4);
    glPopMatrix();
    
    // nose
    glPushMatrix();
    glTranslatef(12.5, 3, 0);
    glutSolidSphere(0.5, 3, 4);
    glPopMatrix();
    
    // mouth
    glPushMatrix();
    glTranslatef(12.8, 1, 0);
    glScaled(0.2, 0.2, 1);
    glutSolidSphere(1, 3, 4);
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
}
