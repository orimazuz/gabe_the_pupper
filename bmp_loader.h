//
//  bmp_loader.h
//  mmn17
//
//  Created by Ori Mazuz on 13/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef bmp_loader_h
#define bmp_loader_h

// loads a bmp file and returns the buffer containing the image.
// the buffer should be deleted after use.
unsigned char * load_bmp(char *filename, unsigned int *width, unsigned int *height);

#endif /* bmp_loader_h */
