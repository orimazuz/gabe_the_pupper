//
//  menu.h
//  mmn17
//
//  Created by Ori Mazuz on 15/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef menu_h
#define menu_h

enum {
    M_HELP,
    M_AMBIENT,
    M_QUIT
};

// show the help picture
void print_help(void);
// show the adjust ambient light menu
void adjust_ambient(void);
// right click menu options.
void menu(int value);

#endif /* menu_h */
