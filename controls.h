//
//  controls.h
//  mmn17
//
//  Created by Ori Mazuz on 15/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef controls_h
#define controls_h

#include "common.h"

// callback for keyboard commands
void keyboard(unsigned char key, int x, int y);
// callback for arrow keys commands
void special_keys(int key, int xx, int yy);
// callback for arrow keys up
void special_up_keys(int key, int xx, int yy);
// callback for when the mouse is pressed and dragged
void motion(int x, int y);
// callback for mouse click
void mouse(int button, int state, int x, int y);
// declarations for views and player initializations.
void init_first_person(void);
void init_third_person(void);
void switch_dog_player(void);
void switch_lamp1_player(void);
void switch_lamp2_player(void);
void toggle_head(void);
void toggle_tail(void);

#endif /* controls_h */
