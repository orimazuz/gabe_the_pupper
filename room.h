//
//  room.h
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef room_h
#define room_h

#define FLOOR_SIZE (60.0)
#define ROOM_HEIGHT (50.0)

// draw a room with a "shiny" (transperent) floor.
void drawRoom(float floor_alpha);

#endif /* room_h */
