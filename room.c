//
//  room.c
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#include "common.h"
#include "room.h"

extern unsigned int floor_tex_width;
extern unsigned int floor_tex_height;
extern unsigned char *floor_tex;
extern unsigned int wall_tex_width;
extern unsigned int wall_tex_height;
extern unsigned char *wall_tex;
extern GLfloat ambient_intensity;

void drawRoom(float floor_alpha)
{
    GLfloat walls_diff[] = {0.5, 0.5, 0.5, floor_alpha};
    GLfloat walls_spec[] = {0, 0, 0, 0};
    GLfloat floor_spec[] = {0.1, 0.1, 0.1, 0.5};
    GLfloat walls_amb[] = 
        {walls_diff[0] * ambient_intensity, walls_diff[1] * ambient_intensity, walls_diff[2] * ambient_intensity, 1};
    
    glMaterialfv(GL_FRONT, GL_DIFFUSE, walls_diff);
    glMaterialfv(GL_FRONT, GL_AMBIENT, walls_amb);
    glMaterialfv(GL_FRONT, GL_SPECULAR, floor_spec);
    
    glPushMatrix();
    glEnable(GL_TEXTURE_2D);
    // the floor should be transperent according to the floor_alpha parameter
    // so we could see the reflection.
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, floor_tex_width, floor_tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, floor_tex);
    
    // floor
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0);
    glNormal3i(0, 1, 0);
    glVertex3f(-FLOOR_SIZE, 0.0, FLOOR_SIZE);
    glTexCoord2f(0.0, 1);
    glNormal3i(0, 1, 0);
    glVertex3f(FLOOR_SIZE, 0.0, FLOOR_SIZE);
    glTexCoord2f(1, 1);
    glNormal3i(0, 1, 0);
    glVertex3f(FLOOR_SIZE, 0.0, -FLOOR_SIZE);
    glTexCoord2f(1, 0.0);
    glNormal3i(0, 1, 0);
    glVertex3f(-FLOOR_SIZE, 0.0, -FLOOR_SIZE);
    glEnd();
    
    glDisable(GL_BLEND);
    
    // walls
    glMaterialfv(GL_FRONT, GL_SPECULAR, walls_spec);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, wall_tex_width, wall_tex_height, 0, GL_RGB, GL_UNSIGNED_BYTE, wall_tex);
    
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 1.0);
    glNormal3i(1, 0, 0);
    glVertex3f(-FLOOR_SIZE, ROOM_HEIGHT, FLOOR_SIZE);
    glTexCoord2f(0.0, 2);
    glNormal3i(1, 0, 0);
    glVertex3f(-FLOOR_SIZE, 0, FLOOR_SIZE);
    glTexCoord2f(2, 2);
    glNormal3i(1, 0, 0);
    glVertex3f(-FLOOR_SIZE, 0, -FLOOR_SIZE);
    glTexCoord2f(2, 1.0);
    glNormal3i(1, 0, 0);
    glVertex3f(-FLOOR_SIZE, ROOM_HEIGHT, -FLOOR_SIZE);
    glEnd();
    
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 2.0);
    glNormal3i(0, 0, -1);
    glVertex3f(-FLOOR_SIZE, 0, FLOOR_SIZE);
    glTexCoord2f(0.0, 1);
    glNormal3i(0, 0, -1);
    glVertex3f(-FLOOR_SIZE, ROOM_HEIGHT, FLOOR_SIZE);
    glTexCoord2f(2, 1);
    glNormal3i(0, 0, -1);
    glVertex3f(FLOOR_SIZE, ROOM_HEIGHT, FLOOR_SIZE);
    glTexCoord2f(2, 2.0);
    glNormal3i(0, 0, -1);
    glVertex3f(FLOOR_SIZE, 0, FLOOR_SIZE);
    glEnd();
    
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 2.0);
    glNormal3i(-1, 0, 0);
    glVertex3f(FLOOR_SIZE, 0, FLOOR_SIZE);
    glTexCoord2f(0.0, 1);
    glNormal3i(-1, 0, 0);
    glVertex3f(FLOOR_SIZE, ROOM_HEIGHT, FLOOR_SIZE);
    glTexCoord2f(2, 1);
    glNormal3i(-1, 0, 0);
    glVertex3f(FLOOR_SIZE, ROOM_HEIGHT, -FLOOR_SIZE);
    glTexCoord2f(2, 2.0);
    glNormal3i(-1, 0, 0);
    glVertex3f(FLOOR_SIZE, 0, -FLOOR_SIZE);
    glEnd();
    
    glBegin(GL_QUADS);
    glTexCoord2f(2, 2);
    glNormal3i(0, 0, 1);
    glVertex3f(-FLOOR_SIZE, 0, -FLOOR_SIZE);
    glTexCoord2f(0, 2);
    glNormal3i(0, 0, 1);
    glVertex3f(FLOOR_SIZE, 0, -FLOOR_SIZE);
    glTexCoord2f(0, 1);
    glNormal3i(0, 0, 1);
    glVertex3f(FLOOR_SIZE, ROOM_HEIGHT, -FLOOR_SIZE);
    glTexCoord2f(2, 1);
    glNormal3i(0, 0, 1);
    glVertex3f(-FLOOR_SIZE, ROOM_HEIGHT, -FLOOR_SIZE);
    glEnd();
    
    glDisable(GL_TEXTURE_2D);
    
    // ceiling
    glBegin(GL_QUADS);
    glNormal3i(0, -1, 0);
    glVertex3f(-FLOOR_SIZE, ROOM_HEIGHT, -FLOOR_SIZE);;
    glNormal3i(0, -1, 0);
    glVertex3f(FLOOR_SIZE, ROOM_HEIGHT, -FLOOR_SIZE);
    glNormal3i(0, -1, 0);
    glVertex3f(FLOOR_SIZE, ROOM_HEIGHT, FLOOR_SIZE);
    glNormal3i(0, -1, 0);
    glVertex3f(-FLOOR_SIZE, ROOM_HEIGHT, FLOOR_SIZE);
    glEnd();
    
    glPopMatrix();
}
