/***
 * Maman 17 by Ori Mazuz, 311322663
 ***/

#include "common.h"
#include "bmp_loader.h"
#include "doggo.h"
#include "lamp.h"
#include "table.h"
#include "room.h"
#include "controls.h"
#include "menu.h"

// global variables that will be used across all .c files:
const float ROOM_DIM = 5.0f;
GLfloat refreshMills = 1.0/60.0 * 1000; // 60fps
GLuint tex; // global texture variable

// third person camera angle
GLfloat angleX = 0, angleY = 0;

// third person view events
int camera_moving, startx, starty;

// dog parts movement flags
int toggle_dog_moving = 0, toggle_head_moving = 0, toggle_tail_moving = 0;

// movement events flags
int move_left=0, move_right=0, move_foreward=0, move_backward=0;

unsigned int floor_tex_width, floor_tex_height;
unsigned char *floor_tex;
unsigned int wall_tex_width, wall_tex_height;
unsigned char *wall_tex;
unsigned int help_tex_width, help_tex_height;
unsigned char *help_tex;
unsigned int amb_tex_width, amb_tex_height;
unsigned char *amb_tex;

// light attenuation parameters
GLfloat blue_light_atten = 0.01, yellow_light_atten = 0.01, ceiling_light_atten = 0.01;

// view flags
int third_person = 1, first_person = 0;

// positions for all players
GLfloat dog_pos[2] = {0, 0};
GLfloat dog_angle = 0;

GLfloat lamp1_pos[2] = {-40, 40};
GLfloat lamp1_angle = 0;

GLfloat lamp2_pos[2] = {40, -40};
GLfloat lamp2_angle = 0;

// player's position is a pointer to the chosen player.
// default is dog
GLfloat *player_pos = dog_pos, *player_angle = &dog_angle;

// angles of the tail and head of the dog
GLfloat tail_angleY = 0.0f, tail_angleZ = 0.0f;
GLfloat head_angleY = 0.0f, head_angleZ = 0.0f;

// player chosen flags
int player_dog = 1, player_lamp1 = 0, player_lamp2 = 0;

// show menues flags
int menu_help = 0, menu_ambient = 0;

GLfloat ambient_intensity = 0.2;

// initialize resources for the game - textures, lights, and other opengl parameters.
void init(void)
{
    GLfloat diffLightColor[] = {0.8, 0.8, 0.8, 1};
    GLfloat specLightColor[] = {0.1, 0.1, 0.1, 1};
    GLfloat lampLightColor1[] = {0.6, 0.6, 0.25, 1}; // yellowish
    GLfloat lampSpecLightColor1[] = {0.2, 0.2, 0.1, 1};
    GLfloat lampLightColor2[] = {0.25, 0.25, 0.7, 1}; // blueish
    GLfloat lampSpecLightColor2[] = {0.1, 0.1, 0.2, 1};
    
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set display-window color to white.
    glClearDepth(1.0f);
    glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);  // Nice perspective corrections
    
    // init texture
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    floor_tex = load_bmp("floor.bmp", &floor_tex_width, &floor_tex_height);
    if (!floor_tex)
        printf("Error loading floor texture.\n");
    
    wall_tex = load_bmp("wall.bmp", &wall_tex_width, &wall_tex_height);
    if (!wall_tex)
        printf("Error loading wall texture.\n");
    
    help_tex = load_bmp("help.bmp", &help_tex_width, &help_tex_height);
    if (!help_tex)
        printf("Error loading help texture.\n");
    
    amb_tex = load_bmp("amb.bmp", &amb_tex_width, &amb_tex_height);
    if (!amb_tex)
        printf("Error loading ambient chooser texture.\n");
    
    // init lighting
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffLightColor);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specLightColor);
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, ceiling_light_atten);
    glEnable(GL_LIGHT0);
    
    // lamp lights
    glLightfv(GL_LIGHT1, GL_DIFFUSE, lampLightColor1);
    glLightfv(GL_LIGHT1, GL_SPECULAR, lampSpecLightColor1);
    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.01);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, yellow_light_atten);
    glEnable(GL_LIGHT1);
    
    glLightfv(GL_LIGHT2, GL_DIFFUSE, lampLightColor2);
    glLightfv(GL_LIGHT2, GL_SPECULAR, lampSpecLightColor2);
    glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 0.1);
    glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, blue_light_atten);
    glEnable(GL_LIGHT2);
    
    glEnable(GL_LIGHTING);
}

// calculate player's position based on the key events.
void calc_player_pos()
{
    if (toggle_tail_moving) {
        if (move_left) {
            tail_angleZ -= 15;
        }
        if (move_right) {
            tail_angleZ += 15;
        }
        if (move_foreward) {
            tail_angleY -= 15;
            if (tail_angleY < 0)
                tail_angleY = 0;
        }
        if (move_backward) {
            tail_angleY += 15;
            if (tail_angleY > 200)
                tail_angleY = 200;
        }
    }
    else if (toggle_head_moving) {
        if (move_left) {
            head_angleZ -= 5;
            if (head_angleZ < -45)
                head_angleZ = -45;
        }
        if (move_right) {
            head_angleZ += 5;
            if (head_angleZ > 45)
                head_angleZ = 45;
        }
        if (move_foreward) {
            head_angleY += 5;
            if (head_angleY > 45)
                head_angleY = 45;
        }
        if (move_backward) {
            head_angleY -= 5;
            if (head_angleY < -10)
                head_angleY = -10;
        }
    }
    else {
        if (move_right) {
            *player_angle -= 5;
        }
        if (move_left) {
            *player_angle += 5;
        }
        if (move_foreward) {
            player_pos[0] += 1.5 * cos(*player_angle * PI / 180.0);
            player_pos[1] -= 1.5 * sin(*player_angle * PI / 180.0);
            if (FLOOR_SIZE - WALL_BUFFER < player_pos[0])
                player_pos[0] = FLOOR_SIZE - WALL_BUFFER;
            if (-(FLOOR_SIZE - WALL_BUFFER) > player_pos[0])
                player_pos[0] = -(FLOOR_SIZE - WALL_BUFFER);
            if (FLOOR_SIZE - WALL_BUFFER < player_pos[1])
                player_pos[1] = FLOOR_SIZE - WALL_BUFFER;
            if (-(FLOOR_SIZE - WALL_BUFFER) > player_pos[1])
                player_pos[1] = -(FLOOR_SIZE - WALL_BUFFER);
        }
        if (move_backward) {
            player_pos[0] -= 1 * cos(*player_angle * PI / 180.0);
            player_pos[1] += 1 * sin(*player_angle * PI / 180.0);
            if (FLOOR_SIZE - WALL_BUFFER < player_pos[0])
                player_pos[0] = FLOOR_SIZE - WALL_BUFFER;
            if (-(FLOOR_SIZE - WALL_BUFFER) > player_pos[0])
                player_pos[0] = -(FLOOR_SIZE - WALL_BUFFER);
            if (FLOOR_SIZE - WALL_BUFFER < player_pos[1])
                player_pos[1] = FLOOR_SIZE - WALL_BUFFER;
            if (-(FLOOR_SIZE - WALL_BUFFER) > player_pos[1])
                player_pos[1] = -(FLOOR_SIZE - WALL_BUFFER);
        }
    }
}

// disply function is called every 1/60 seconds (60fps)
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear color and depth buffers
    glMatrixMode(GL_MODELVIEW);     // To operate on model-view matrix

    // set ceiling light position
    GLfloat lightPosition[] = {0, ROOM_HEIGHT - 5, 0, 1};
    
    // set lights' position based on the lamps' position.
    GLfloat lightPosition1[] = {lamp1_pos[0], 20, lamp1_pos[1], 1};
    GLfloat lightPosition2[] = {lamp2_pos[0], 20, lamp2_pos[1], 1};
    
    // set ambient light intensity based on user's preference.
    GLfloat ambient_light[] = {ambient_intensity, ambient_intensity, ambient_intensity, 1};
    
    // set lamps' light intensity based on user's decisions.
    GLfloat yellow_coefficient = 1 - (yellow_light_atten - MIN_ATTEN)/(MAX_ATTEN - MIN_ATTEN);
    GLfloat blue_coefficient = 1 - (blue_light_atten - MIN_ATTEN)/(MAX_ATTEN - MIN_ATTEN);

    // set the emmition of the lamps' materials.
    GLfloat lamp_emmition1[] = {1 * yellow_coefficient, 1 * yellow_coefficient, 0.5 * yellow_coefficient , 0};
    GLfloat lamp_emmition2[] = {0.5 * blue_coefficient, 0.5 * blue_coefficient, 1 * blue_coefficient, 0};

    // set light intensity (attenuation)
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, ceiling_light_atten);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, yellow_light_atten);
    glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, blue_light_atten);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_light);

    // save current state
    glPushMatrix();
    
    // on third person view, perform scene rotations based on user mouse input.
    if (third_person) {
        glRotatef(angleY, 1.0, 0.0, 0.0);
        glRotatef(angleX, 0.0, 1.0, 0.0);
    }
    
    // on first person view, look at the direction that the player is facing.
    if (first_person) {
        glLoadIdentity();
        GLfloat y_center = 10;
        if (player_dog) {
            y_center += sin(head_angleY * PI / 180);
            glRotatef(head_angleZ, 0.0, 1.0, 0.0);
        }

        gluLookAt(player_pos[0], 10.0, player_pos[1],
                  player_pos[0] + cos(*player_angle * PI / 180),
                  y_center,
                   player_pos[1] - sin(*player_angle * PI / 180),
                  0.0, 1.0, 0.);
    }
    
    // claculate player's position based on the keys pressed.
    calc_player_pos();
    
    // position the lights
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glLightfv(GL_LIGHT1, GL_POSITION, lightPosition1);
    glLightfv(GL_LIGHT2, GL_POSITION, lightPosition2);
    
    // draw lamp 1 and it's reflection
    glPushMatrix();
    // move lamp 1 to it's position and rotate with it's angle.
    glTranslatef(lamp1_pos[0], 0, lamp1_pos[1]);
    glRotatef(lamp1_angle, 0, 1, 0);
    drawLamp(lamp_emmition1);
    
    // draw lamp 1 reflection
    glPushMatrix();
    glScalef(1.0, -1.0, 1.0);
    drawLamp(lamp_emmition1);
    glPopMatrix();
    
    glPopMatrix();
    
    // draw lamp 2 and it's reflection
    glPushMatrix();
    glTranslatef(lamp2_pos[0], 0, lamp2_pos[1]);
    glRotatef(lamp2_angle, 0, 1, 0);
    drawLamp(lamp_emmition2);
    
    // reflection
    glPushMatrix();
    glScalef(1.0, -1.0, 1.0);
    drawLamp(lamp_emmition2);
    glPopMatrix();
    
    glPopMatrix();
    
    // draw table & tea pot and their reflections
    glPushMatrix();
    glTranslatef(-30, 0, -30);
    drawTable();
    
    // reflection
    glPushMatrix();
    glScalef(1.0, -1.0, 1.0);
    drawTable();
    glPopMatrix();
    
    glPopMatrix();
    
    // draw the dog and it's reflection
    glPushMatrix();
    // move the dog to it's position and rotate it with it's angle.
    glTranslatef(dog_pos[0], 0, dog_pos[1]);
    glRotatef(dog_angle, 0, 1, 0);
    drawDoggo();
    
    // reflection
    glPushMatrix();
    glScalef(1.0, -1.0, 1.0);
    drawDoggo();
    glPopMatrix();
    
    glPopMatrix();
    
    // floor alpha is greater when the lights are stronger so we can see the reflections better.
    float floor_alpha = 1 + 0.646667 -
                ((1 - ((yellow_light_atten - MIN_ATTEN) * 10)) +
                 (1 - ((blue_light_atten - MIN_ATTEN) * 10)) +
                 (1 - ((ceiling_light_atten - MIN_ATTEN) * 5))) / 3;
    drawRoom(floor_alpha);
    
    glPopMatrix();
    
    // print menues if the user wanted to.
    if (menu_help)
        print_help();
    if (menu_ambient)
        adjust_ambient();
    
    glutSwapBuffers();  // Swap the front and back frame buffers (double buffering)
}

void timer(int value)
{
    glutPostRedisplay();      // Post re-paint request to activate display()
    glutTimerFunc(refreshMills, timer, 0); // next timer call milliseconds later
}

// initialize global third person view.
void init_third_person()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0, 90.0, 50.0,
              0.0, 0.0, 0.0,
              0.0, 1.0, 0.);
    
    first_person = 0;
    third_person = 1;
}

// initialize first person view.
// the view depends on the current player.
void init_first_person()
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(player_pos[0], 10.0, player_pos[1],
              player_pos[0] + cos(*player_angle * PI / 180), 10.0, player_pos[1] - sin(*player_angle * PI / 180),
              0.0, 1.0, 0.);
    first_person = 1;
    third_person = 0;
}

// set the dog as the player.
void switch_dog_player()
{
    player_pos = dog_pos;
    player_angle = &dog_angle;
    player_dog = 1;
    player_lamp1 = 0;
    player_lamp2 = 0;
    toggle_dog_moving = 1;
    toggle_head_moving = 0;
    toggle_tail_moving = 0;
}

// set lamp1 (yellow) as the player.
void switch_lamp1_player()
{
    player_pos = lamp1_pos;
    player_angle = &lamp1_angle;
    player_dog = 0;
    player_lamp1 = 1;
    player_lamp2 = 0;
    toggle_dog_moving = 0;
    toggle_head_moving = 0;
    toggle_tail_moving = 0;
}

// set lamp2 (blue) as the player.
void switch_lamp2_player()
{
    player_pos = lamp2_pos;
    player_angle = &lamp2_angle;
    player_dog = 0;
    player_lamp1 = 0;
    player_lamp2 = 1;
    toggle_dog_moving = 0;
    toggle_head_moving = 0;
    toggle_tail_moving = 0;
}

// uppon calling this function, the head moves with the arrows.
void toggle_head()
{
    toggle_dog_moving = 0;
    toggle_head_moving = 1;
    toggle_tail_moving = 0;
}

// upon calling this function, the tail moves with the arrows.
void toggle_tail()
{
    toggle_dog_moving = 0;
    toggle_head_moving = 0;
    toggle_tail_moving = 1;
}

// resizing not allowed
void resize(int width, int height)
{
    // we ignore the params and do:
    glutReshapeWindow(1080, 720);
}

int main(int argc, char** argv) 
{
	// initialize:
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize (1080, 720);
	glutCreateWindow ("Maman 17 by Ori Mazuz");
	init();
	glutMouseFunc(mouse);
	glutDisplayFunc(display);
    glutMotionFunc(motion);
    glutTimerFunc(0, timer, 0);
    glutSpecialFunc(special_keys);
    glutSpecialUpFunc(special_up_keys);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(resize);
    
    glutCreateMenu(menu);
    
    glutAddMenuEntry("Help", M_HELP);
    glutAddMenuEntry("Adjust ambient light", M_AMBIENT);
    glutAddMenuEntry("Quit", M_QUIT);
    
    glutAttachMenu(GLUT_RIGHT_BUTTON);
    
    glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
    glLoadIdentity();             // Reset
    glViewport(0, 0, 1080, 720);
    
    // Enable perspective projection with fovy, aspect, zNear and zFar
    gluPerspective(45.0f, 1080.0/720.0, 15.6f, 250.0f);
    
    init_third_person();
    switch_dog_player();
    
	glutMainLoop();
	return 0;
}
