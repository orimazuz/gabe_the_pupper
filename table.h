//
//  table.h
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef table_h
#define table_h

// draw a table with a metalic teapot on it.
void drawTable(void);

#endif /* table_h */
