EXE = mmn17
CFLAGS = -O3 -Wno-deprecated-declarations
PLATFORM = $(shell uname)
OBJ = main.o bmp_loader.o controls.o doggo.o lamp.o table.o room.o table.o menu.o
DEP = common.h bmp_loader.h controls.h doggo.h lamp.h table.h room.h table.h menu.h
CC = gcc

# Linux
ifeq ($(PLATFORM), Linux)
		LDFLAGS = -lGL -lGLU -lglut -lm

# OS X
else ifeq ($(PLATFORM), Darwin)
		LDFLAGS = -framework OpenGL -framework GLUT -Wno-deprecated-declarations
endif

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(EXE) : $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

clean :
	rm -f *.o $(EXE)
