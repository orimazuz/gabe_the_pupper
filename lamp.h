//
//  lamp.h
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef lamp_h
#define lamp_h

// draw a lamp that emmits light.
void drawLamp(float *emmition);

#endif /* lamp_h */
