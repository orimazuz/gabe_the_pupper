//
//  doggo.h
//  mmn17
//
//  Created by Ori Mazuz on 14/06/2018.
//  Copyright © 2018 Ori Mazuz. All rights reserved.
//

#ifndef doggo_h
#define doggo_h

// draw the whole dog according to it's head/tail/legs position.
void drawDoggo(void);

#endif /* doggo_h */
